<?php
/**
 * Article.php - manipulates the content of articles, or creates new ones or deletes articles.
 * 
 * @author Bugslayer
 * 
 */
// Include required external scripts
require_once dirname ( __FILE__ ) . '/../components/db.php';
include_once dirname ( __FILE__ ) . '/../components/formvalidationtools.php';

global $action;
// Determine and process the action.
switch ($action) {
	case "save" :
		$id = save_article ();
		header ( "Location: index.php?action=show&page=article&id=" . $id );
		break;
	case "delete" :
		delete_article ();
		header ( "Location: index.php" );
		break;
	default :
		die ( "Illegal action" );
}

/*
 * Removes an article from the database
 */
function delete_article() {
	global $mysqli;
	if (! isset ( $_GET ['id'] ))
		die ( 'No article to delete' );
	$id = $_GET ['id'];
	$sql = 'DELETE FROM ARTICLE WHERE ID=' . $id . ';';
	$mysqli->query ( $sql );
}

/*
 * Adds or modifies an article
 */
function save_article() {
	global $mysqli;
	if (! isset ( $_POST ['ID'] )) {
		die ( 'Error in form' );
	}
	$vals = array ();
	// Check the ID
	$id = $mysqli->real_escape_string ( $_POST ['ID'] );
	$isNew = false;
	if ($id != 0) {
		// This should be an existing article
		// So, check it first
		$sql = "SELECT Name FROM ARTICLE WHERE ID=" . $id . ";";
		$result = $mysqli->query ( $sql );
		if (! $result || $result->num_rows == 0)
			die ( 'Article not found' );
		$vals ['ID'] = $id;
	} else {
		$isNew = true;
		$vals ['ID'] = 'NULL';
	}
	
	// Check the Name
	if (isset ( $_POST ['Name'] )) {
		$vals ['Name'] = "'" . $_POST ['Name'] . "'";
	} else {
		$vals ['Name'] = 'NULL';
	}
	
	// Check the Content
	if (isset ( $_POST ['Content'] )) {
		$vals ['Content'] = "'" . $_POST ['Content'] . "'";
	} else {
		$vals ['Content'] = 'NULL';
	}
	
	// Update the database
	if ($isNew) {
		$sql = "INSERT INTO ARTICLE (Name, Content) VALUES (" . $vals ['Name'] . ", " . $vals ['Content'] . ");";
		if (! $mysqli->query ( $sql )) {
			printf ( "Errormessage: %s\n", $mysqli->error );
		}
		return $mysqli->insert_id;
	} else {
		$sql = "UPDATE ARTICLE SET Name=" . $vals ['Name'] . ", Content=" . $vals ['Content'] . " WHERE ID=" . $vals ['ID'] . ";";
		if (! $mysqli->query ( $sql )) {
			printf ( "Errormessage: %s\n", $mysqli->error );
		}
		return $id;
	}
}

?>